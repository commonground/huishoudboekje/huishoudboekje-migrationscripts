import os

import sqlalchemy as sa
from dotenv import load_dotenv
from os.path import join, dirname
from sqlalchemy import text
import logging

path = join(dirname(__file__), '.env')
load_dotenv(path)

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level="INFO",
    datefmt='%Y-%m-%d %H:%M:%S')


def get_alarm_ids():
    engine = sa.create_engine(os.environ.get("DB_ALARMENSERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting alarms..')
            curr = conn.execute(text('SELECT uuid FROM alarms'))
            alarms = curr.fetchall()
            curr.close()
            list = []
            for alarm in alarms:
                list.append(str(alarm[0]))
            return list
        except Exception as e:
            logging.exception(e)

def get_afspraken():
    engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting afspraken..')
            curr = conn.execute(text('SELECT id, alarm_id FROM afspraken WHERE alarm_id IS NOT NULL'))
            afspraken = curr.fetchall()
            curr.close()
            return afspraken
        except Exception as e:
            logging.exception(e)



def create_new_alarms():
    alarms = get_alarm_ids()
    afspraken = get_afspraken()
    total_alarms = len(afspraken)
    logging.info(f"Checking {total_alarms} afspraken")
    
    afspraken_with_wrong_alarm_id = []

    for afspraak in afspraken:
        (id, alarm_id) = afspraak
        
        if str(alarm_id) not in alarms:
            afspraken_with_wrong_alarm_id.append(id)

    logging.info(f"Found {len(afspraken_with_wrong_alarm_id)} afspraken with invalid alarm id")

create_new_alarms()
logging.info("Done checking")
