import ast
from datetime import datetime
import io
import os
import uuid

import pandas as pd
import psycopg2
import sqlalchemy as sa
from dotenv import load_dotenv
from os.path import join, dirname
from sqlalchemy import text
import logging

path = join(dirname(__file__), '.env')
load_dotenv(path)

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level="INFO",
    datefmt='%Y-%m-%d %H:%M:%S')


def get_postaddressen():
    engine = sa.create_engine(os.environ.get("DB_POSTADRESSENSERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting postaddressen..')
            curr = conn.execute(text(
                'SELECT id, street, "houseNumber", "postalCode", "locality" FROM "Address"'))
            data = curr.fetchall()
            curr.close()
            return data
        except Exception as e:
            logging.exception(e)


def get_burgers():
    engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting burgers..')
            curr = conn.execute(text(
                'SELECT id, telefoonnummer, email, geboortedatum, achternaam, huisnummer, postcode, straatnaam, voorletters, voornamen, plaatsnaam, huishouden_id, bsn, saldo, uuid, huishouden_uuid, saldo_alarm, end_date, start_date FROM burgers'))
            data = curr.fetchall()
            curr.close()
            return data
        except Exception as e:
            logging.exception(e)


def get_rekeningen():
    engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting rekeningen..')
            curr = conn.execute(text(
                'SELECT id, iban, rekeninghouder, uuid FROM rekeningen'))
            data = curr.fetchall()
            curr.close()
            return data
        except Exception as e:
            logging.exception(e)


def get_rekeningen_burgers():
    engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting rekeningen burgers..')
            curr = conn.execute(text(
                'SELECT rekening_id, burger_id FROM rekening_burger'))
            data = curr.fetchall()
            curr.close()
            return data
        except Exception as e:
            logging.exception(e)


def get_rekeningen_departments():
    engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting rekeningen afdelingen..')
            curr = conn.execute(text(
                'SELECT rekening_id, afdeling_uuid FROM afdeling_rekening'))
            data = curr.fetchall()
            curr.close()
            return data
        except Exception as e:
            logging.exception(e)


def get_huishoudens():
    engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting huishoudens..')
            curr = conn.execute(text(
                'SELECT id, uuid FROM huishoudens'))
            data = curr.fetchall()
            curr.close()
            return data
        except Exception as e:
            logging.exception(e)
            

def get_departments():
    engine = sa.create_engine(os.environ.get("DB_PARTYSERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting departments..')
            curr = conn.execute(text(
                'SELECT uuid, postadressen_ids, rekeningen_ids FROM departments'))
            data = curr.fetchall()
            curr.close()
            return data
        except Exception as e:
            logging.exception(e)

def get_csv_buffer(data):
    df = pd.DataFrame(data)
    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer, index=False, header=True)
    csv_buffer.seek(0)
    return csv_buffer


def process_households(households, party_conn):
    if households is None or len(households) == 0:
        logging.info(f"No households to migrate, skipping...")
        return
    total = len(households)
    logging.info(f"Processing {total} households")

    insert_data_households = []
    households_id_to_uuid_dict = {}

    for household in households:
        (id, household_uuid) = household

        insert_data_households.append({
            "uuid": household_uuid,
        })

        households_id_to_uuid_dict.update({id: household_uuid})

    csv_bufffer = get_csv_buffer(insert_data_households)

    # Perform raw SQL using psycopg2
    with party_conn.cursor() as cur:
        logging.info(f"Inserting {total} households...")
        cur.copy_expert("""
            COPY households (uuid)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_bufffer)

    return households_id_to_uuid_dict

def get_hhb_number(num):
    return f"HHB{num:07d}"

def process_burgers(burgers, huishouden_id_to_uuid_dict, party_conn):
    if burgers is None or len(burgers) == 0:
        logging.info(f"No burgers to migrate, skipping...")
        return
    total = len(burgers)
    logging.info(f"Processing {total} burgers")

    insert_data_burgers = []
    insert_data_addresses = []
    burgers_id_to_uuid_dict = {}

    for burger in burgers:
        (id, telefoonnummer, email, geboortedatum, achternaam, huisnummer, 
         postcode, straatnaam, voorletters, voornamen, plaatsnaam, huishouden_id, 
         bsn, saldo, burger_uuid, huishouden_uuid, saldo_alarm, end_date, start_date) = burger
        
        unix_birth_day = int(geboortedatum.timestamp()) if geboortedatum is not None and geboortedatum != '' else None
        unix_end_date= int(end_date.timestamp()) if end_date is not None and end_date != '' else None

        address_uuid = uuid.uuid4()
        insert_data_addresses.append({
            "uuid": address_uuid,
            "street": straatnaam,
            "house_number": huisnummer,
            "postal_code": postcode,
            "city": plaatsnaam
        })

        insert_data_burgers.append({
            "uuid": burger_uuid,
            "hhb_number": get_hhb_number(id),
            "phone_number": telefoonnummer,
            "email": email,
            "surname": achternaam,
            "first_names": voornamen,
            "infix": " ",
            "initials": voorletters,
            "address_id": address_uuid,
            "household_id": huishouden_id_to_uuid_dict[huishouden_id],
            "bsn": bsn,
            "use_saldo_alarm": saldo_alarm,
            "end_date":  None if unix_end_date is None else str(unix_end_date),
            "start_date": start_date,
            "birth_date": unix_birth_day
        })

        burgers_id_to_uuid_dict.update({id: burger_uuid})

    csv_bufffer_addresses = get_csv_buffer(insert_data_addresses)
    csv_bufffer_burgers = get_csv_buffer(insert_data_burgers)

    # Perform raw SQL using psycopg2
    with party_conn.cursor() as cur:
        logging.info(f"Inserting {total} addresses...")
        cur.copy_expert("""
            COPY addresses(uuid, street, house_number, postal_code, city)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_bufffer_addresses)

        logging.info(f"Inserting {total} citizens...")
        cur.copy_expert("""
            COPY citizens(uuid, hhb_number, phone_number, email, surname, first_names, infix, initials, address_id, household_id, bsn, use_saldo_alarm, end_date, start_date, birth_date)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_bufffer_burgers)

        cur.execute("""
            UPDATE citizens
            SET infix = ''
        """)

    return burgers_id_to_uuid_dict


def process_accounts(accounts, party_conn):
    if accounts is None or len(accounts) == 0:
        logging.info(f"No accounts to migrate, skipping...")
        return
    total = len(accounts)
    logging.info(f"Processing {total} accounts")

    insert_data_accounts= []
    accounts_id_to_uuid_dict = {}

    for account in accounts:
        (id, iban, rekeninghouder, account_uuid) = account

        insert_data_accounts.append({
            "uuid": account_uuid,
            "iban": iban,
            "account_holder": rekeninghouder,
        })

        accounts_id_to_uuid_dict.update({id: account_uuid})

    csv_bufffer = get_csv_buffer(insert_data_accounts)

    # Perform raw SQL using psycopg2
    with party_conn.cursor() as cur:
        logging.info(f"Inserting {total} accounts...")
        cur.copy_expert("""
            COPY accounts(uuid, iban, account_holder)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_bufffer)

    return accounts_id_to_uuid_dict


def process_citizen_accounts(ciizen_accounts, citizen_dict, account_dict, party_conn):
    if ciizen_accounts is None or len(ciizen_accounts) == 0:
        logging.info(f"No ciizen accounts to migrate, skipping...")
        return
    total = len(ciizen_accounts)
    logging.info(f"Processing {total} ciizen accounts")

    insert_data_ciizen_accounts= []

    for ciizen_account in ciizen_accounts:
        (rekening_id, burger_id) = ciizen_account

        insert_data_ciizen_accounts.append({
            "citizen_id": citizen_dict[burger_id],
            "account_id": account_dict[rekening_id]
        })

    csv_bufffer = get_csv_buffer(insert_data_ciizen_accounts)

    # Perform raw SQL using psycopg2
    with party_conn.cursor() as cur:
        logging.info(f"Inserting {total} ciizen accounts...")
        cur.copy_expert("""
            COPY citizen_accounts(citizen_id, account_id)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_bufffer)


def process_addresses(addresses, party_conn):
    if addresses is None or len(addresses) == 0:
        logging.info(f"No addresses to migrate, skipping...")
        return
    total = len(addresses)
    logging.info(f"Processing {total} addresses")

    address_ids = set()

    insert_data_addresses = []

    for address in addresses:
        (id, street, houseNumber, postalCode, locality) = address

        address_ids.add(id)

        insert_data_addresses.append({
            "uuid": id,
            "street": street,
            "house_number": houseNumber,
            "postal_code": postalCode,
            "city": locality
        })

    csv_bufffer_addresses = get_csv_buffer(insert_data_addresses)

    # Perform raw SQL using psycopg2
    with party_conn.cursor() as cur:
        logging.info(f"Inserting {total} addresses...")
        cur.copy_expert("""
            COPY addresses(uuid, street, house_number, postal_code, city)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_bufffer_addresses)

    return address_ids


def process_department_accounts_and_addresses(departments, account_dict, address_ids, party_conn):
    if departments is None or len(departments) == 0:
        logging.info(f"No departments to update, skipping...")
        return
    total = len(departments)
    logging.info(f"Processing {total} departments")

    insert_data_department_addresses = []
    insert_data_department_accounts = []

    for department in departments:
        (department_uuid, postadressen_ids, rekeningen_ids) = department
        postadressen_list = ast.literal_eval(postadressen_ids)
        for postaddress in postadressen_list:
            if postaddress in address_ids:
                insert_data_department_addresses.append({
                    "department_uuid": department_uuid,
                    "address_uuid": postaddress
                })
        
        rekeningen_list = ast.literal_eval(rekeningen_ids)
        for account in rekeningen_list:
            insert_data_department_accounts.append({
                "department_id": department_uuid,
                "account_id": account_dict[account]
            })

    csv_bufffer_addresses = get_csv_buffer(insert_data_department_addresses)
    csv_bufffer_accounts = get_csv_buffer(insert_data_department_accounts)

    # Perform raw SQL using psycopg2
    with party_conn.cursor() as cur:
        logging.info(f"Inserting {len(insert_data_department_addresses)} department addresses...")
        cur.copy_expert("""
            COPY department_addresses(department_uuid, address_uuid)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_bufffer_addresses)

        logging.info(f"Inserting {len(insert_data_department_accounts)} department accounts...")
        cur.copy_expert("""
            COPY department_accounts(department_id, account_id)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_bufffer_accounts)

def update_afspraken_burgers(burger_id_dict, hhb_update_conn):
    logging.info(f"Updating afspraken burger ids to uuids")
    if burger_id_dict is None or len(burger_id_dict) == 0:
        logging.info(f"No burger ids to update, skipping...")
        return
    total_updates_burgers = len(burger_id_dict)
    logging.info(f"Updating {total_updates_burgers} burger ids:")

    update_ids_table = []

    for id, uuid in burger_id_dict.items():
        update_ids_table.append({
            "id": id,
            "uuid": uuid,
        })

    csv_bufffer_burgers = get_csv_buffer(update_ids_table)

    # Perform raw SQL using psycopg2
    with hhb_update_conn.cursor() as cur:
        logging.info(f"Creating temporary table...")
        cur.execute("""
            CREATE TEMP TABLE temp_uuid_updates (
                id INT,
                uuid UUID
            )
        """)
        cur.copy_expert("""
            COPY temp_uuid_updates(id,uuid)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_bufffer_burgers)

        logging.info(f"Updating afspraken burger ids using temporary table...")
        cur.execute("""
            UPDATE afspraken
            SET burger_uuid = temp_uuid_updates.uuid
            FROM temp_uuid_updates
            WHERE afspraken.burger_id = temp_uuid_updates.id
        """)

        logging.info(f"Dropping temporary table...")
        cur.execute("DROP TABLE temp_uuid_updates;")

def update_afspraken_accounts(account_id_dict, hhb_update_conn):
    logging.info(f"Updating afspraken account ids to uuids")
    if account_id_dict is None or len(account_id_dict) == 0:
        logging.info(f"No account ids to update, skipping...")
        return
    total_updates = len(account_id_dict)
    logging.info(f"Updating {total_updates} account ids:")

    update_ids_table = []

    for id, uuid in account_id_dict.items():
        update_ids_table.append({
            "id": id,
            "uuid": uuid,
        })

    csv_bufffer_burgers = get_csv_buffer(update_ids_table)

    # Perform raw SQL using psycopg2
    with hhb_update_conn.cursor() as cur:
        logging.info(f"Creating temporary table...")
        cur.execute("""
            CREATE TEMP TABLE temp_uuid_updates (
                id INT,
                uuid UUID
            )
        """)
        cur.copy_expert("""
            COPY temp_uuid_updates(id,uuid)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_bufffer_burgers)

        logging.info(f"Updating afspraken account ids using temporary table...")
        cur.execute("""
            UPDATE afspraken
            SET tegen_rekening_uuid = temp_uuid_updates.uuid
            FROM temp_uuid_updates
            WHERE afspraken.tegen_rekening_id = temp_uuid_updates.id
        """)

        logging.info(f"Dropping temporary table...")
        cur.execute("DROP TABLE temp_uuid_updates;")

def create_new_data():
    with psycopg2.connect(os.environ.get("DB_PARTYSERVICE_URL")) as party_conn: 
        with psycopg2.connect(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL")) as hhb_update_conn:
            households_id_dict =  process_households(get_huishoudens(), party_conn)
            burger_id_dict = process_burgers(get_burgers(), households_id_dict, party_conn)
            account_id_dict = process_accounts(get_rekeningen(), party_conn)
            process_citizen_accounts(get_rekeningen_burgers(), burger_id_dict, account_id_dict, party_conn)
            address_ids = process_addresses(get_postaddressen(), party_conn)
            process_department_accounts_and_addresses(get_departments(), account_id_dict, address_ids, party_conn)
            update_afspraken_burgers(burger_id_dict, hhb_update_conn)
            update_afspraken_accounts(account_id_dict, hhb_update_conn)

        party_conn.commit()
        hhb_update_conn.commit()


logging.info("start")
create_new_data()
logging.info("end")
