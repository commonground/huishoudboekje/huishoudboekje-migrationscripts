import binascii
import hashlib
import os
import uuid
import pandas as pd
import io
import sqlalchemy as sa
from sqlalchemy import text
import psycopg2
from dotenv import load_dotenv
from os.path import join, dirname
import logging
from datetime import datetime

from sqlalchemy.dialects.mysql import insert

path = join(dirname(__file__), '.env')
load_dotenv(path)

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level="INFO",
    datefmt='%Y-%m-%d %H:%M:%S')


def get_customer_statement_messages():
    engine = sa.create_engine(os.environ.get("DB_BANKSERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting old csms..')
            curr = conn.execute(text(
                'SELECT id, upload_date, raw_data, transaction_reference_number, account_identification, filename, uuid FROM customer_statement_messages'))
            csms = curr.fetchall()
            curr.close()
            return csms
        except Exception as e:
            logging.exception(e)


def get_transactions():
    engine = sa.create_engine(os.environ.get("DB_BANKSERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting old transactions..')
            curr = conn.execute(text(
                'SELECT id, customer_statement_message_id, information_to_account_owner, bedrag, is_credit, tegen_rekening, transactie_datum, is_geboekt, uuid FROM bank_transactions'))
            transactions = curr.fetchall()
            curr.close()
            return transactions
        except Exception as e:
            logging.exception(e)


def get_overschrijvingen():
    engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info("Getting old paymentrecords...")
            curr = conn.execute(text(
                'SELECT o.id, o.afspraak_id, o.datum, o.bedrag, o.bank_transaction_id, o.uuid, a.uuid, e.uuid, a.omschrijving, r.iban, r.rekeninghouder, e.verwerking_datum FROM overschrijvingen AS o JOIN afspraken AS a ON o.afspraak_id = a.id JOIN export AS e on o.export_id = e.id JOIN rekeningen as r on a.tegen_rekening_id = r.id'))
            records = curr.fetchall()
            curr.close()
            return records
        except Exception as e:
            logging.exception(e)

def get_exports():
    engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info("Getting old exports...")
            curr = conn.execute(text(
                "SELECT uuid, naam, timestamp, eind_datum, start_datum, xmldata, sha256, verwerking_datum FROM export"
            ))
            exports = curr.fetchall()
            curr.close()
            return exports

        except Exception as e:
            logging.exception(e)


def process_raw_data(raw_data):
    bytes = raw_data.encode('utf-8')
    size = len(bytes)
    sha256 = hashlib.sha256(bytes).hexdigest()
    return bytes, size, sha256


def create_payment_records(records, bank_conn):
    if records is None or len(records) == 0:
        logging.info(f"No payment records to migrate, skipping...")
        return
    total_records = len(records)
    logging.info(f"Processing {total_records} payment records")

    insert_payment_records = []

    for record in records:
        (id, agreement_id, date, amount, bank_transaction_id, uuid, agreeement_uuid, export_uuid, description, account_iban, account_name, verwerking_datum) = record
        original_processing_date_unix = int(datetime.strptime(str(date), '%Y-%m-%d %H:%M:%S').timestamp())
        processing_date_unix = original_processing_date_unix
        if verwerking_datum:
            processing_date_unix = int(datetime.strptime(str(verwerking_datum), '%Y-%m-%d %H:%M:%S').timestamp())

        insert_payment_records.append({
            "uuid": uuid,
            "amount" : -amount,
            "created_at": int(datetime.now().timestamp()),
            "original_processing_date" : original_processing_date_unix,
            "payment_export_uuid" :  export_uuid,
            "agreement_uuid" : agreeement_uuid,
            "processing_date" : processing_date_unix,
            "account_name" : account_name,
            "account_iban" : account_iban,
            "description" : description,
            # It was decided to not care about historic unreconciled payment records so we just say they are reconciled here
            # If for any reason this changes in the future then get all reconciled = True AND transaction_uuid = None records and match them to transactions.
            # Matching transactions is easiest to do on first agreement uuid, then the amount and date. (Is matched to this agreement? > has the same amount & date as the payment record)
            "reconciled" : True,
            "transaction_uuid" : None
        })
    
    df = pd.DataFrame(insert_payment_records)

    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer, index=False, header=True)
    csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with bank_conn.cursor() as cur:
        logging.info(f"Inserting {total_records} payment records...")
        cur.copy_expert("""
            COPY paymentrecords(uuid,amount,created_at,original_processing_date,payment_export_uuid,agreement_uuid,processing_date,account_name,account_iban,description,reconciled,transaction_uuid)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_buffer)

def create_payment_exports(exports, bank_conn, file_conn):
    if exports is None or len(exports) == 0:
        logging.info(f"No export files to migrate, skipping...")
        return
    total_exports = len(exports)
    logging.info(f"Processing {total_exports} payment exports")

    insert_exports = []
    insert_files = []

    for export in exports:
        (_uuid, naam, timestamp, eind_datum, start_datum, xmldata, old_sha256, verwerking_datum) = export
        unix_start_date = int(datetime.strptime(str(start_datum), '%Y-%m-%d %H:%M:%S').timestamp())
        unix_end_date = int(datetime.strptime(str(eind_datum), '%Y-%m-%d %H:%M:%S').timestamp())
        file_uuid = uuid.uuid4()

        bytes, size, sha256 = process_raw_data(xmldata)
        if sha256 != old_sha256:
            sha256 = old_sha256
            logging.info(f"file {file_uuid} had a different original sha256 then expected,  using old sha256 for historic purposes")

        insert_exports.append({
            "uuid" : _uuid,
            "created_at": unix_start_date,
            "start_Date": unix_start_date,
            "end_date": unix_end_date,
            "file_uuid" : file_uuid,
            "sha256" : sha256
        })

        insert_files.append({
            "uuid" : file_uuid,
            "sha256" : sha256,
            "name" : naam + '.xml',
            "last_modified" : int(datetime.now().timestamp()),
            "uploaded_at": unix_start_date,
            "size" : size,
            "bytes" : '\\x' + bytes.hex(),
            "type": 2
        })


    
    export_df = pd.DataFrame(insert_exports)

    export_csv_buffer = io.StringIO()
    export_df.to_csv(export_csv_buffer, index=False, header=True)
    export_csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with bank_conn.cursor() as cur:
        logging.info(f"Inserting {total_exports} exports...")
        cur.copy_expert("""
            COPY paymentexports(uuid,created_at,"start_Date",end_date,file_uuid,sha256)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, export_csv_buffer)

    file_df = pd.DataFrame(insert_files)

    file_csv_buffer = io.StringIO()
    file_df.to_csv(file_csv_buffer, index=False, header=True)
    file_csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with file_conn.cursor() as file_cur:
        logging.info(f"Inserting {total_exports} files...")
        file_cur.copy_expert("""
            COPY files(uuid,sha256,name,last_modified,uploaded_at,size,bytes,type)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, file_csv_buffer)

def create_customer_statement_messages(csms, bank_conn, file_conn):
    if csms is None or len(csms) == 0:
        logging.info(f"No customer statement messages to migrate, skipping...")
        return
    total_csms = len(csms)
    logging.info(f"Processing {total_csms} customer statement messages")

    insert_data_csms = []
    insert_data_files = []
    file_hash_list = []
    csms_id_to_uuid_dict = {}

    for csm in csms:

        (id, upload_date, raw_data, transaction_reference_number,
         account_identification, filename, csm_uuid) = csm

        unix_date = int(datetime.strptime(str(upload_date), '%Y-%m-%d %H:%M:%S').timestamp())
        file_uuid = uuid.uuid4()
        bytes, size, sha256 = process_raw_data(raw_data)

        if sha256 in file_hash_list:
            raise Exception("Duplicate files found :( the devs need to discuss how we will handle this)")
        else:
            file_hash_list.append(sha256)

        insert_data_files.append({
            "uuid": file_uuid,
            "sha256": sha256,
            "name": filename,
            "last_modified": unix_date,
            "uploaded_at": unix_date,
            "size": size,
            "bytes": '\\x' + bytes.hex(),
            "type": 1
        })

        insert_data_csms.append({
            "uuid": csm_uuid,
            "transaction_reference": transaction_reference_number,
            "account_identification": account_identification,
            "file_uuid": file_uuid,
            "uploaded_at": unix_date
        })

        csms_id_to_uuid_dict.update({id: csm_uuid})    

    df = pd.DataFrame(insert_data_csms)

    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer, index=False, header=True)
    csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with bank_conn.cursor() as cur:
        logging.info(f"Inserting {total_csms} cms's ...")
        cur.copy_expert("""
            COPY customerstatementmessages(uuid,transaction_reference,account_identification,file_uuid, uploaded_at)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_buffer)

    file_df = pd.DataFrame(insert_data_files)

    file_csv_buffer = io.StringIO()
    file_df.to_csv(file_csv_buffer, index=False, header=True)
    file_csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with file_conn.cursor() as file_cur:
        logging.info(f"Inserting {total_csms} files...")
        file_cur.copy_expert("""
            COPY files(uuid,sha256,name,last_modified,uploaded_at,size,bytes,type)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, file_csv_buffer)


    return csms_id_to_uuid_dict


def create_transactions(transactions, csms_id_to_uuid_dict, bank_conn):
    if transactions is None or len(transactions) == 0:
        logging.info(f"No transactions to migrate, skipping...")
        return
    total_transactions = len(transactions)
    logging.info(f"Processing {total_transactions} transactions")

    insert_data_transactions = []

    for transaction in transactions:

        (id, customer_statement_message_id, information_to_account_owner, bedrag,
         is_credit, tegen_rekening, transactie_datum, is_geboekt, uuid) = transaction

        unix_date = int(datetime.strptime(str(transactie_datum), '%Y-%m-%d %H:%M:%S').timestamp())

        if (tegen_rekening is None or tegen_rekening == "") and (information_to_account_owner is None or information_to_account_owner == ""):
            information_owner = "interbancaire transactie"
        else:
            information_owner = information_to_account_owner;

        insert_data_transactions.append({
            "uuid": uuid,
            "amount": bedrag,
            "is_credit": is_credit,
            "from_account": tegen_rekening,
            "date": unix_date,
            "information_to_account_owner": information_owner,
            "is_reconciled": is_geboekt,
            "customer_statement_message": csms_id_to_uuid_dict[customer_statement_message_id]
        })

    df = pd.DataFrame(insert_data_transactions)

    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer, index=False, header=True)
    csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with bank_conn.cursor() as cur:
        logging.info(f"Inserting {total_transactions} transactions...")
        cur.copy_expert("""
            COPY transactions(uuid,amount,is_credit,from_account,date,information_to_account_owner,is_reconciled,customer_statement_message)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_buffer)


def update_journaalposten(old_transactions, hhb_update_conn):
    logging.info(f"Updating journaalpost transaction ids to uuids")
    if old_transactions is None or len(old_transactions) == 0:
        logging.info(f"No transaction ids to update, skipping...")
        return
    total_transactions = len(old_transactions)
    logging.info(f"Updating {total_transactions} transaction ids")


    update_ids_table = []

    for transaction in old_transactions:
        (id, customer_statement_message_id, information_to_account_owner, bedrag,
         is_credit, tegen_rekening, transactie_datum, is_geboekt, uuid) = transaction

        update_ids_table.append({
            "id": id,
            "uuid": uuid,
        })

    df = pd.DataFrame(update_ids_table)

    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer, index=False, header=True)
    csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with hhb_update_conn.cursor() as cur:
        logging.info(f"Creating temporary table...")
        cur.execute("""
            CREATE TEMP TABLE temp_uuid_updates (
                id INT,
                uuid UUID
            )
        """)
        cur.copy_expert("""
            COPY temp_uuid_updates(id,uuid)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_buffer)

        logging.info(f"Updating journaalposten using temporary table...")
        cur.execute("""
            UPDATE journaalposten
            SET transaction_uuid = temp_uuid_updates.uuid
            FROM temp_uuid_updates
            WHERE journaalposten.transaction_id = temp_uuid_updates.id
        """)

        logging.info(f"Dropping temporary table...")
        cur.execute("DROP TABLE temp_uuid_updates;")

def create_new_data():
    with psycopg2.connect(os.environ.get("DB_BANKSERVICE_URL"))  as bank_conn:
        with psycopg2.connect(os.environ.get("DB_FILESERVICE_URL"))  as file_conn:
            with psycopg2.connect(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))  as hhb_update_conn:
                csms_id_to_uuid_dict = create_customer_statement_messages(get_customer_statement_messages(), bank_conn, file_conn)
                logging.info("created new csms")
                old_transactions = get_transactions()
                create_transactions(old_transactions, csms_id_to_uuid_dict, bank_conn)
                logging.info("created new transactions")
                update_journaalposten(old_transactions, hhb_update_conn)
                logging.info("updated journaalpost transaction uuids")
                create_payment_exports(get_exports(), bank_conn, file_conn)
                logging.info("created new payment exports")
                create_payment_records(get_overschrijvingen(), bank_conn)

                bank_conn.commit()
                file_conn.commit()
                hhb_update_conn.commit()


logging.info("start")
create_new_data()
logging.info("end")
