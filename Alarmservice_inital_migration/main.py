import os

import pytz
import sqlalchemy as sa
from dotenv import load_dotenv
from os.path import join, dirname
from sqlalchemy import text
import logging
import ast
import json
from datetime import datetime, timedelta

path = join(dirname(__file__), '.env')
load_dotenv(path)

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level="INFO",
    datefmt='%Y-%m-%d %H:%M:%S')

weekdays = {"monday": 1, "tuesday": 2, "wednesday": 3, "thursday": 4, "friday": 5, "saturday": 6, "sunday": 0}


def get_old_alarms():
    engine = sa.create_engine(os.environ.get("DB_ALARMENSERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting old alarms..')
            curr = conn.execute(text('SELECT * FROM "Alarm" where "isActive" = true'))
            alarms = curr.fetchall()
            curr.close()
            return alarms
        except Exception as e:
            logging.exception(e)

def get_alarm_afspraken_dict():
    engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting afspraken..')
            curr = conn.execute(text('SELECT * FROM afspraken'))
            afspraken = curr.fetchall()
            curr.close()
            dict = {}
            for afspraak in afspraken:
                (id, burger_id, valid_from, valid_through, aantal_betalingen, tegen_rekening_id, bedrag, credit, omschrijving, rubriek_id, zoektermen, betaalinstructie, afdeling_id, postadres_id, alarm_id, uuid, rubriek_uuid, burger_uuid, afdeling_uuid, tegen_rekening_uuid, postadres_uuid, alarm_uuid) = afspraak
                dict[id] = (credit, valid_through)
            return dict
        except Exception as e:
            logging.exception(e)

def determine_alarm_type(byMonth, byMonthDay, byDay, endDate):
    if endDate is not None and endDate != '':
        return 3
    if byDay != '{}':
        return 2
    if len(byMonth) == 1 and len(byMonthDay) > 0:
        return 4
    if len(byMonth) > 0 and len(byMonthDay) > 0:
        return 1


def determine_check_on(alarm_type, days, byMonth, ByMonthDay, datumMargin):
    today = datetime.today()
    today = datetime(today.year, today.month, today.day, 0, 0, 0)
    result = today

    if alarm_type == 1 or alarm_type == 4 or alarm_type == 3:
        result = monthly_check_on(byMonth,ByMonthDay, datumMargin, today)
    if alarm_type == 2:
        result = weekly_check_on(days, datumMargin, today)

    gmt = pytz.timezone('GMT')
    gmt_midnight = datetime(result.year, result.month, result.day, 0, 0, 0)
    gmt_midnight = gmt.localize(gmt_midnight)
    return int(gmt_midnight.timestamp())


def monthly_check_on(byMonth, ByMonthDay, datumMargin, today):
    day = next((monthDay for monthDay in ByMonthDay if monthDay + datumMargin >= today.day), ByMonthDay[0])
    if day + datumMargin < today.day:
        month = next((month for month in byMonth if month > today.month), byMonth[0])
    else:
        month = next((month for month in byMonth if month >= today.month), byMonth[0])
    year = today.year
    if month < today.month or (month == today.month and day + datumMargin + 1 <= today.day):
        year = year + 1

    date = datetime(year, month, day)
    add_days = datumMargin + 1
    return date + timedelta(add_days)


def weekly_check_on(days, datumMargin, today):
    today_weekday = today.weekday() + 1
    nextDayNumber = next((day for day in days if day + datumMargin >= today_weekday), days[0])
    dayDifference = calculateDaysToNewDayOfWeeks(nextDayNumber, today_weekday)
    if dayDifference > 7:
        dayDifference = dayDifference - 7

    add_days = dayDifference + datumMargin + 1
    return today + timedelta(add_days)

def calculateDaysToNewDayOfWeeks(newDay, oldDay):
    # 1 + 7 (newDayOfWeek - oldDayOfWeek) % 7) -1 gives us the difference in days to reach the next day of week.
    # If we simply subtracted the weeks, we would incorrectly go back in time instead of forwards.
    return 1 + 7 + ((newDay - oldDay) % 7) - 1


def create_new_alarms(alarms):
    afspraken_dict = get_alarm_afspraken_dict()
    total_alarms = len(alarms)
    logging.info(f"found {total_alarms} old alarms")
    logging.info("creating new alarms...")
    engine = sa.create_engine(os.environ.get("DB_ALARMENSERVICE_URL"))
    count = 0
    with engine.begin() as conn:
        for alarm in alarms:
            progress_percentage = (count + 1) / total_alarms * 100
            logging.info(f"Alarm: {count} : Progress:  {progress_percentage}")
            count = count + 1

            (id, afspraakId, signaalId, isActive, datumMargin, bedrag, bedragMargin, byDay,
             byMonth, ByMonthDay, startDate, endDate, afspraakUuid, signaalUuid) = alarm
            alarm_afspraak = afspraken_dict.get(afspraakId, None)
            
            if alarm_afspraak is None:
                logging.info(f"Skipped Alarm, no afspraak found")
                continue

            (credit, valid_trough) = alarm_afspraak

            if startDate is None:
                logging.info(f"Incorrect Alarm found with id: {id}")
                continue
                #logging.info(f"Alarm: {alarm}")

            unix_start = int(datetime.strptime(startDate, '%Y-%m-%d').timestamp())
            unix_end = int(
                datetime.strptime(endDate, '%Y-%m-%d').timestamp()) if endDate is not None and endDate != '' else None
            if unix_end is None:
                unix_end = int(
                    valid_trough.timestamp()) if valid_trough is not None and valid_trough != '' else None
            days = []
            if byDay != '{}':
                days = [weekdays[day] for day in byDay[1:-1].lower().replace(' ', '').split(',')]
            dayStr = json.dumps(days) if len(days) > 0 else None
            monthStr = json.dumps(byMonth) if len(byMonth) > 0 else None
            monthDayStr = json.dumps(ByMonthDay) if len(ByMonthDay) > 0 else None
            alarm_type = determine_alarm_type(byMonth, ByMonthDay, byDay, endDate)
            bedrag = bedrag if credit else bedrag * -1
            stmt = text(
                """INSERT INTO alarms VALUES (:id, :isActive, :datumMargin, :bedrag, :bedragMargin, :monthStr, :monthDayStr, :days, :unix_check_on, :unix_start, :unix_end, :alarm_type)""")
            conn.execute(stmt, {'id': id, 'isActive': isActive, 'datumMargin': datumMargin, 'bedrag': bedrag,
                                'bedragMargin': bedragMargin, 'monthStr': monthStr, 'monthDayStr': monthDayStr,
                                'days': dayStr, 'unix_check_on': determine_check_on(alarm_type, days, byMonth, ByMonthDay, datumMargin) ,'unix_start': unix_start, 'unix_end': unix_end,
                                'alarm_type': alarm_type})
        conn.commit()

create_new_alarms(get_old_alarms())
logging.info("created new alarms")
