import io
import os

import pandas as pd
import psycopg2
import sqlalchemy as sa
from dotenv import load_dotenv
from os.path import join, dirname
from sqlalchemy import text
import logging

path = join(dirname(__file__), '.env')
load_dotenv(path)

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level="INFO",
    datefmt='%Y-%m-%d %H:%M:%S')


def get_organisaties():
    engine = sa.create_engine(os.environ.get("DB_ORGANISATIESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting old organisaties..')
            curr = conn.execute(text(
                'SELECT id, vestigingsnummer, naam, kvknummer, uuid FROM organisaties'))
            organisaties = curr.fetchall()
            curr.close()
            return organisaties
        except Exception as e:
            logging.exception(e)


def get_afdelingen():
    engine = sa.create_engine(os.environ.get("DB_ORGANISATIESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting old transactions..')
            curr = conn.execute(text(
                'SELECT id, naam, organisatie_id, postadressen_ids, rekeningen_ids, uuid FROM afdelingen'))
            afdelingen = curr.fetchall()
            curr.close()
            return afdelingen
        except Exception as e:
            logging.exception(e)


def get_afdelingen_rekeningen():
    engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting rekeningen..')
            curr = conn.execute(text(
                'SELECT rekening_id, afdeling_id FROM rekening_afdeling'))
            rekeningen = curr.fetchall()
            curr.close()
            return rekeningen
        except Exception as e:
            logging.exception(e)


def create_organisations(organisations, party_conn):
    if organisations is None or len(organisations) == 0:
        logging.info(f"No organisaties to migrate, skipping...")
        return
    total_organisations = len(organisations)
    logging.info(f"Processing {total_organisations} organisaties")

    insert_data_organisations = []
    organisation_id_to_uuid_dict = {}

    for organisation in organisations:
        (id, vestigingsnummer, naam, kvknummer, uuid) = organisation
        insert_data_organisations.append({
            "uuid": uuid,
            "name": naam,
            "kvk_number": kvknummer,
            "branch_number": vestigingsnummer
        })

        organisation_id_to_uuid_dict.update({id: uuid})

    df = pd.DataFrame(insert_data_organisations)

    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer, index=False, header=True)
    csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with party_conn.cursor() as cur:
        logging.info(f"Inserting {total_organisations} organisations...")
        cur.copy_expert("""
            COPY organisations(uuid, name, kvk_number, branch_number)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_buffer)

    return organisation_id_to_uuid_dict


def parse_postadressen_ids(postadressen_ids):
    if postadressen_ids is None:
        return '[]'
    return '[' + ','.join(f'"{element}"' for element in postadressen_ids) + ']'


def parse_rekeningen_ids(rekeningen_ids):
    if rekeningen_ids is None:
        return '[]'
    return '[' + ','.join(f'{element}' for element in rekeningen_ids) + ']'


def create_departments(departmetnts, organisation_id_to_uuid_dict, party_conn):
    if departmetnts is None or len(departmetnts) == 0:
        logging.info(f"No afdelingen to migrate, skipping...")
        return
    total_departmetnts = len(departmetnts)
    logging.info(f"Processing {total_departmetnts} departments")

    insert_data_departmetnts = []
    departmetnts_id_to_uuid_dict = {}

    for department in departmetnts:
        (id, naam, organisatie_id, postadressen_ids, rekeningen_ids, uuid) = department
        departmetnts_id_to_uuid_dict.update({id: uuid})

        insert_data_departmetnts.append({
            "uuid": uuid,
            "name": naam,
            "postadressen_ids": parse_postadressen_ids(postadressen_ids),
            "rekeningen_ids": parse_rekeningen_ids(rekeningen_ids),
            "organisation_uuid": organisation_id_to_uuid_dict[organisatie_id]
        })

    df = pd.DataFrame(insert_data_departmetnts)

    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer, index=False, header=True)
    csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with party_conn.cursor() as cur:
        logging.info(f"Inserting {total_departmetnts} departments...")
        cur.copy_expert("""
            COPY departments(uuid, name, postadressen_ids, rekeningen_ids, organisation_uuid)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_buffer)
    
    return departmetnts_id_to_uuid_dict


def update_afspraken(departmetnts_id_to_uuid_dict, hhb_update_conn):
    logging.info(f"Updating afspraken afdeling ids to uuids")
    if departmetnts_id_to_uuid_dict is None or len(departmetnts_id_to_uuid_dict) == 0:
        logging.info(f"No afdeling ids to update, skipping...")
        return
    total_updates = len(departmetnts_id_to_uuid_dict)
    logging.info(f"Updating {total_updates} afdeling ids:")

    update_ids_table = []

    for id, uuid in departmetnts_id_to_uuid_dict.items():
        update_ids_table.append({
            "id": id,
            "uuid": uuid,
        })

    df = pd.DataFrame(update_ids_table)

    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer, index=False, header=True)
    csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with hhb_update_conn.cursor() as cur:
        logging.info(f"Creating temporary table...")
        cur.execute("""
            CREATE TEMP TABLE temp_uuid_updates (
                id INT,
                uuid UUID
            )
        """)
        cur.copy_expert("""
            COPY temp_uuid_updates(id,uuid)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_buffer)

        logging.info(f"Updating afspraken afdeling ids using temporary table...")
        cur.execute("""
            UPDATE afspraken
            SET afdeling_uuid = temp_uuid_updates.uuid
            FROM temp_uuid_updates
            WHERE afspraken.afdeling_id = temp_uuid_updates.id
        """)

        logging.info(f"Dropping temporary table...")
        cur.execute("DROP TABLE temp_uuid_updates;")


def update_rekening_afdelingen(departmetnts_id_to_uuid_dict, hhb_update_conn):
    rekeningen = get_afdelingen_rekeningen()

    logging.info(f"Updating rekening afdeling ids to uuids")
    if rekeningen is None or len(rekeningen) == 0:
        logging.info(f"No rekening afdeling ids to update, skipping...")
        return
    total_updates = len(rekeningen)
    logging.info(f"Updating {total_updates} rekening afdeling ids")

    insert_data = []

    for rekening in rekeningen:

        (rekening_id, afdeling_id) = rekening

        insert_data.append({
            "rekening_id": rekening_id,
            "afdeling_uuid": departmetnts_id_to_uuid_dict[afdeling_id]
        })

    df = pd.DataFrame(insert_data)

    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer, index=False, header=True)
    csv_buffer.seek(0) 

    # Perform raw SQL using psycopg2
    with hhb_update_conn.cursor() as cur:
        logging.info(f"Inserting {total_updates} afdeling rekeningen...")
        cur.copy_expert("""
            COPY afdeling_rekening(rekening_id, afdeling_uuid)
            FROM STDIN WITH (FORMAT csv, HEADER)
        """, csv_buffer)


def create_new_data():
    with psycopg2.connect(os.environ.get("DB_PARTYSERVICE_URL")) as party_conn:
        with psycopg2.connect(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL")) as hhb_update_conn:
            organisation_id_to_uuid_dict = create_organisations(get_organisaties(), party_conn)
            logging.info("moved organisations in party service")
            departmetnts_id_to_uuid_dict = create_departments(get_afdelingen(), organisation_id_to_uuid_dict, party_conn)
            logging.info("moved departments in party service")
            update_afspraken(departmetnts_id_to_uuid_dict, hhb_update_conn)
            logging.info("updated afspraken afdeling uuids")
            update_rekening_afdelingen(departmetnts_id_to_uuid_dict, hhb_update_conn)
            logging.info("updated rekeningen afdeling uuids")
           
            party_conn.commit()
            hhb_update_conn.commit()


logging.info("start")
create_new_data()
logging.info("end")
