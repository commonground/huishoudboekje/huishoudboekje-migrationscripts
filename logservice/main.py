import os
import logging
from dotenv import load_dotenv
from sqlalchemy import create_engine, text

logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level="INFO",
        datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)
logger.info(f"Starting {__name__}")

load_dotenv()
conenction_string = os.environ.get("DB_LOGSERVICE_URL")

engine = create_engine(conenction_string)
connection = engine.connect()
logger.info(f"Connected to database")

transaction = connection.begin()
try:
    count_gebruikers_activiteiten_query = text("""
        SELECT COUNT(*) FROM gebruikersactiviteiten;
    """)

    total_gebruikers_activiteiten = connection.execute(count_gebruikers_activiteiten_query).scalar()

    logger.info(f"Found a total of {total_gebruikers_activiteiten} gebruikers activitetien in table gebruikersactiviteiten")

    gebruikers_activiteiten_into_user_activities_query = text("""
        INSERT INTO user_activities (uuid, timestamp, user_id, action, snapshot_before, snapshot_after, meta)
        SELECT uuid, EXTRACT(epoch FROM timestamp), gebruiker_id, action, CAST(snapshot_before AS TEXT), CAST(snapshot_after AS TEXT), CAST(meta AS TEXT)
        FROM gebruikersactiviteiten
    """)

    inserted_rows = connection.execute(gebruikers_activiteiten_into_user_activities_query).rowcount

    if total_gebruikers_activiteiten != inserted_rows:
        logger.error(f"Inserted {inserted_rows} user activities when {total_gebruikers_activiteiten} were found")
        logger.error(f"Aborting, starting rollback...")
        raise Exception("Inserted rows does not match original count")

    logger.info(f"Inserted {inserted_rows} user activities")
    
    insert_entities_query = text("""
        INSERT INTO user_activity_entities (user_activity_uuid, entity_id, entity_type)
        SELECT uuid, entityId,entityType
        FROM (
            SELECT
                uuid::UUID as uuid,
                (json->>'entityId')::TEXT as entityId,
                (json->>'entityType')::TEXT as entityType
            FROM (
                SELECT
                    uuid,
                    jsonb_array_elements(REPLACE(BTRIM(entities::TEXT, '\"'),'''', '\"')::jsonb) AS json
                FROM gebruikersactiviteiten) 
            AS parseEntities) AS selectQuery
        WHERE uuid IS NOT NULL AND entityId IS NOT NULL AND entityType IS NOT NULL AND LENGTH(entityId) <= 36""")

    inserted_rows = connection.execute(insert_entities_query).rowcount
    
    if inserted_rows <= 0:
        logger.error(f"Did not insert any entites, this is unlikely")
        logger.error(f"Aborting, starting rollback...")
        raise Exception("Did not insert any entites")
    
    logger.info(f"Inserted user activities entities")
    
    transaction.commit()

except Exception as e:
    transaction.rollback()
    logger.error(f"Something went wrong, transaction has been rolled back")
    logger.error(e)

finally:
    logger.info(f"Closing connection")
    connection.close()
    logger.info(f"Completed migration script")