import hashlib
import os
import uuid

import sqlalchemy as sa
from dotenv import load_dotenv
from os.path import join, dirname
from sqlalchemy import text
import logging
from datetime import datetime

from sqlalchemy.dialects.mysql import insert

path = join(dirname(__file__), '.env')
load_dotenv(path)

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level="INFO",
    datefmt='%Y-%m-%d %H:%M:%S')


def get_files():
    engine = sa.create_engine(os.environ.get("DB_FILESERVICE_URL"))
    with engine.connect() as conn:
        try:
            logging.info('Getting files..')
            curr = conn.execute(text(
                'SELECT uuid, uploaded_at FROM files'))
            files = curr.fetchall()
            curr.close()
            return files
        except Exception as e:
            logging.exception(e)


def update_csms(files, bank_update_conn):
    logging.info(f"Updating CSM's with uploaded at")
    if files is None or len(files) == 0:
        logging.info(f"No CSM's to update, skipping...")
        return
    total_updates = len(files)
    logging.info(f"Updating {total_updates} CSM's")
    count = 0


    update_cases = []

    for file in files:
        progress_percentage = (count + 1) / total_updates * 100
        logging.info(f"CSM update: {count} : Progress:  {progress_percentage}")
        count = count + 1

        (uuid, uploaded_at) = file

        update_cases.append(f" WHEN '{uuid}' THEN {uploaded_at} ")

    update_cases_string = "".join(update_cases)

    query = text(f"""
        UPDATE customerstatementmessages
        SET uploaded_at = CASE file_uuid
            {update_cases_string}
            END
        WHERE file_uuid IS NOT NULL
    """)

    bank_update_conn.execute(query)



def update():
    bank_update_engine = sa.create_engine(os.environ.get("DB_BANKSERVICE_URL"))
    with bank_update_engine.begin() as bank_update_engine:
        update_csms(get_files(), bank_update_engine)
        logging.info("updated CSM's with uploaded at")
        bank_update_engine.commit()


logging.info("start")
update()
logging.info("end")
